package com.julie.runningtracker.utils

enum class SortType {
    DATE, RUNNING_TIME, AVERAGE_SPEED, DISTANCE, CALORIES_BURNT
}