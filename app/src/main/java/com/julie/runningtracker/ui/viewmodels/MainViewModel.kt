package com.julie.runningtracker.ui.viewmodels

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.julie.runningtracker.db.Run
import com.julie.runningtracker.repositories.MainRepository
import com.julie.runningtracker.utils.SortType
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(
        val mainRepository: MainRepository
) : ViewModel() {

    private val runsSortedByDate = mainRepository.getAllRunsSortedByDate()

    private val runsSortedByDistance = mainRepository.getAllRunsSortedByDistance()
    private val runsSortedByCaloriesBurnt = mainRepository.getAllRunsSortedByCaloriesBurnt()
    private val runsSortedByTimeInMillis = mainRepository.getAllRunsSortedByTimeInMillis()
    private val runsSortedByAverageSpeed = mainRepository.getAllRunsSortedByAverageSpeed()

    val runs = MediatorLiveData<List<Run>>()

    var sortType = SortType.DATE

    init {
        runs.addSource(runsSortedByDate) { result ->
            if (sortType == SortType.DATE) {
                result?.let { runs.value = it }
            }
        }
        runs.addSource(runsSortedByAverageSpeed) { result ->
            if (sortType == SortType.AVERAGE_SPEED) {
                result?.let { runs.value = it }
            }
        }
        runs.addSource(runsSortedByCaloriesBurnt) { result ->
            if (sortType == SortType.CALORIES_BURNT) {
                result?.let { runs.value = it }
            }
        }
        runs.addSource(runsSortedByDistance) { result ->
            if (sortType == SortType.DISTANCE) {
                result?.let { runs.value = it }
            }
        }
        runs.addSource(runsSortedByTimeInMillis) { result ->
            if (sortType == SortType.RUNNING_TIME) {
                result?.let { runs.value = it }
            }
        }
    }


    fun sortRuns(sortType: SortType) =
            when(sortType){
                SortType.DATE -> runsSortedByDate.value?.let{runs.value = it}
                SortType.DISTANCE -> runsSortedByDistance.value?.let{runs.value = it}
                SortType.AVERAGE_SPEED -> runsSortedByAverageSpeed.value?.let{runs.value = it}
                SortType.CALORIES_BURNT -> runsSortedByCaloriesBurnt.value?.let{runs.value = it}
                SortType.RUNNING_TIME -> runsSortedByTimeInMillis.value?.let{runs.value = it}

            }.also {
                this.sortType = sortType
            }

    fun insertRun(run: Run) = viewModelScope.launch {
        mainRepository.insertRun(run)
    }


}